#!/usr/bin/env python3

"""
This script is used to bring (update) the project's dependencies info from their .kde-ci.yaml files, so it is available to kde-builder.

Usage:
    export REPOS=/home/username/kde/src
    export REPO_METADATA=/home/username/.local/state/sysadmin-repo-metadata

    regenerate_dependencies.py --branch-group kf6-qt6

Parameters:
    `--branch-group`: Mandatory argument. Supported values: `kf6-qt6`, `stable-kf6-qt6`, `kf5-qt5`, `stable-kf5-qt5`.
    `--list`: Get a list of branches used for projects.
    `--lms-br-diff`: Print differences in branch maps from lms.json vs from branch-rules.yml. If there is any difference, exits with 1, else exits with 0.
"""


import sys
import argparse
import os
import pathlib
import re
from dataclasses import dataclass
import yaml


@dataclass
class MetaData:
    repopath: str
    projectpath: str
    repoactive: bool


@dataclass
class Dependencies:
    on: list[str]
    require: dict


@dataclass
class KdeCi:
    dependencies: list[Dependencies] | None
    runtime_dependencies: list[Dependencies] | None


@dataclass
class BranchGroups:
    kf5_qt5: str | None
    kf6_qt6: str | None
    stable_kf5_qt5: str | None
    stable_kf6_qt6: str | None


class RepoMetadata:
    def __repr__(self):
        return f"{{path: {self.path}, projects: {self.projects}}}"

    def __init__(self, path: str):
        self.path: str = path
        self.projects: list[MetaData]

        ignored_categories = [
            "websites",
            "wikitolearn",
            "webapps",
            "historical",
            "documentation",
            "sysadmin",
            "neon",
            "packaging",
            "unmaintained",
        ]

        entries: list[MetaData] = []
        projects_invent = pathlib.Path(f"{path}/projects-invent")
        projects_invent = projects_invent.rglob("*.yaml")
        projects_invent = [x for x in projects_invent if x.parts[-1 - 2] not in ignored_categories]
        for project in projects_invent:
            with open(project, "r") as f:
                content = yaml.safe_load(f)
            entries.append(MetaData(content["repopath"], content["projectpath"], content["repoactive"]))
        sorted_entries = sorted(entries, key=lambda x: x.repopath)
        self.projects = sorted_entries

    def logical_module_structure(self) -> dict[str, BranchGroups]:
        with open(f"{self.path}/dependencies/logical-module-structure.json", "r") as f:
            json = yaml.safe_load(f)

        groups = json["groups"]
        weighted_keys: list[ProjectKey] = get_project_keys(groups)
        result: dict[str, BranchGroups] = {}

        for project in self.projects:
            for key in weighted_keys:
                if key.glob.endswith("*"):
                    fixed_regexp = key.glob.replace("*", ".*")
                    pattern = re.compile(rf"{fixed_regexp}")
                else:
                    pattern = re.compile(rf"{key.glob}$")  # this prevents matching line with the extra symbols, for example for "extragear/office/kbibtex-testset" to not match for "extragear/office/kbibtex"

                if re.match(pattern, project.projectpath):
                    b: BranchGroups = groups[key.glob]
                    result[project.projectpath] = b
                    break

        return result

    def branch_rules(self):
        with open(f"{self.path}/branch-rules.yml", "r") as f:
            bryml = yaml.safe_load(f)

        layers = bryml.keys()
        result = {}
        for layer in layers:
            weighted_keys = get_project_keys(bryml[layer])
            for project in self.projects:
                for key in weighted_keys:
                    if key.glob.endswith("*"):
                        fixed_regexp = key.glob.replace("*", ".*")
                        pattern = re.compile(rf"{fixed_regexp}")
                    else:
                        pattern = re.compile(rf"{key.glob}$")  # this prevents matching line with the extra symbols, for example for "office/kbibtex-testset" to not match for "office/kbibtex"

                    if re.match(pattern, project.projectpath):
                        branch = bryml[layer][key.glob]
                        if project.projectpath not in result:
                            result[project.projectpath] = {}
                        result[project.projectpath][layer] = branch
                        break
        return result


@dataclass
class ProjectKey:
    glob: str
    weight: int


def get_project_keys(groups: dict) -> list[ProjectKey]:
    # We need the most specific glob to win, e.g. kde/workspace/* must win over kde/*,
    # and kde/workspace/foo must win over kde/workspace/*

    weighted_keys: list[ProjectKey] = []
    for key in groups.keys():
        weight = None

        if "*" not in key:
            # Exact match is the highest weight
            weight = 10
        else:
            # Prefer more specific matches
            weight = key.count("/")

        weighted_keys.append(ProjectKey(key, weight))

    weighted_keys.sort(key=lambda x: x.weight, reverse=True)
    return weighted_keys


def get_dependencies(input_dependencies, branch_group: str) -> list[str]:
    result: list[str] = []

    for dep in input_dependencies:
        def should_include() -> bool:
            if "@all" in dep["on"]:
                return True

            if "Linux" in dep["on"]:
                return True

            if "qt6" not in branch_group and "Linux/Qt5" in dep["on"]:
                return True

            if "qt6" in branch_group and "Linux/Qt6" in dep["on"]:
                return True

            return False

        if not should_include():
            continue

        for key in dep["require"]:
            result.append(key)
    return result


def list_dependencies(project: MetaData, branch_group: str) -> list[str]:
    base_name = project.repopath.split("/")[1]

    kdeci_file = f"""{os.environ["REPOS"]}/{base_name}/.kde-ci.yml"""

    try:
        with open(kdeci_file, "r"):
            pass
    except FileNotFoundError:
        return []

    with open(kdeci_file, "r") as f:
        content = yaml.safe_load(f)
        kdeci = KdeCi(content.get("Dependencies", []), content.get("RuntimeDependencies", []))

    dependencies = get_dependencies(kdeci.dependencies, branch_group)
    runtime_dependencies = get_dependencies(kdeci.runtime_dependencies, branch_group)

    dependencies.extend(runtime_dependencies)
    return dependencies


def main():
    repo_metadata = RepoMetadata(os.environ["REPO_METADATA"])
    lms = repo_metadata.logical_module_structure()
    br = repo_metadata.branch_rules()

    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument("--branch-group", required=True)
    arg_parser.add_argument("--list", action="store_true")
    arg_parser.add_argument("--lms-br-diff", action="store_true")
    args = arg_parser.parse_args()

    if args.list:
        for project in repo_metadata.projects:
            if not project.repoactive:
                continue

            pr_groups = lms.get(project.projectpath, {})

            match args.branch_group:
                case "kf5-qt5":
                    branch = pr_groups.get("kf5-qt5")
                case "kf6-qt6":
                    branch = pr_groups.get("kf6-qt6")
                case "stable-kf5-qt5":
                    branch = pr_groups.get("stable-kf5-qt5")
                case "stable-kf6-qt6":
                    branch = pr_groups.get("stable-kf6-qt6")
                case _:
                    raise ValueError

            if branch is None:
                branch = "<default>"
            elif branch == "":
                branch = "<disabled>"

            print(f"{project.repopath} {branch}")
        sys.exit()

    if args.lms_br_diff:
        found_diff = False
        for project in repo_metadata.projects:
            if not project.repoactive:
                continue

            pr_groups = lms.get(project.projectpath, {})
            br_groups = br.get(project.projectpath, {})

            lms_branch_kf5 = pr_groups.get("kf5-qt5", "<default>")
            lms_branch_kf6 = pr_groups.get("kf6-qt6", "<default>")
            lms_branch_stable_kf5 = pr_groups.get("stable-kf5-qt5", "<default>")
            lms_branch_stable_kf6 = pr_groups.get("stable-kf6-qt6", "<default>")

            br_branch_kf5 = br_groups.get("@latest", "<default>")
            br_branch_kf6 = br_groups.get("@latest-kf6", "<default>")
            br_branch_stable_kf5 = br_groups.get("@stable", "<default>")
            br_branch_stable_kf6 = br_groups.get("@stable-kf6", "<default>")

            def check_diff(lms_layer_name, lms_branch, br_layer_name, br_branch):
                if lms_branch == "":
                    lms_branch = "<disabled>"
                if br_branch == "":
                    br_branch = "<disabled>"

                if lms_branch != br_branch:
                    nonlocal found_diff
                    found_diff = True
                    print(f"{project.repopath} [{lms_layer_name} {br_layer_name}] {lms_branch} {br_branch}")

            check_diff("kf5-qt5", lms_branch_kf5, "@latest", br_branch_kf5)
            check_diff("kf6-qt6", lms_branch_kf6, "@latest-kf6", br_branch_kf6)
            check_diff("stable-kf5-qt5", lms_branch_stable_kf5, "@stable", br_branch_stable_kf5)
            check_diff("stable-kf6-qt6", lms_branch_stable_kf6, "@stable-kf6", br_branch_stable_kf6)

        if found_diff:
            sys.exit(1)
        else:
            sys.exit(0)

    qt6_ignored_projects = [
        "frameworks/khtml",
        "frameworks/kdelibs4support",
        "frameworks/kdewebkit",
        "frameworks/kemoticons",
        "frameworks/kinit",
        "frameworks/kjs",
        "frameworks/kjsembed",
        "frameworks/kmediaplayer",
        "frameworks/kross",
        "frameworks/kdesignerplugin",
        "frameworks/kxmlrpcclient",
        "libraries/kross-interpreters",
        "libraries/kwebkitpart",
    ]

    ignored_deps = {
        "plasma/kwin": ["plasma/plasma-workspace"],
        "plasma/kscreenlocker": ["plasma/plasma-workspace"],
        "plasma/plasma-workspace": ["plasma/powerdevil"],
        "sdk/selenium-webdriver-at-spi": ["plasma/kwin"],
    }

    print("# This file has been auto-generated from .kde-ci.yml files")
    print("# Instead of editing it manually re-run the generation")

    for project in repo_metadata.projects:
        if "qt6" in args.branch_group and project.repopath in qt6_ignored_projects:
            continue

        if not project.repoactive:
            continue

        deps = list_dependencies(project, args.branch_group)

        print()
        print(f"# {project.repopath}")
        print("# This data was autogenerated from the project's .kde-ci.yml file. DO NOT MODIFY. Add new dependencies to the .kde-ci.yml files instead.")

        for dep in deps:
            projectpath = None

            if "qt6" in args.branch_group and dep in qt6_ignored_projects:
                continue

            if project.repopath in ignored_deps and dep in ignored_deps.get(project.repopath):
                continue

            if "third-party" in dep:
                projectpath = dep
            else:
                maybe_project = next((p for p in repo_metadata.projects if p.repopath == dep), None)

                if maybe_project is not None:
                    projectpath = maybe_project.projectpath
                else:
                    raise ValueError(f"{project.projectpath} requests non-existent dependency {dep}")

            print(f"{project.projectpath}: {projectpath}")


if __name__ == "__main__":
    main()
