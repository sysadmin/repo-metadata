#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2024 Andrew Shark <ashark@linuxcomp.ru>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""
Exit with non-zero status if ksb module-definitions and yaml build-configs files content (for kdesrc-build config and kde-builder config correspondingly) differs.
"""

# pylint: disable=unspecified-encoding, f-string-without-interpolation, unnecessary-dict-index-lookup, too-many-branches, too-many-statements, too-many-nested-blocks
# pylint: disable=too-few-public-methods, consider-using-dict-items, unnecessary-pass, missing-function-docstring, redefined-outer-name, consider-using-with

import sys
import os
from typing import TextIO
import re
import yaml


class KSBParser:
    """
    Read the config in legacy ksb format.
    """

    @staticmethod
    def read_ksb_file(config_path: str) -> dict:
        """
        Read in the settings from the configuration.

        Args:
            config_path: Full path of the config file to read from.
        """
        ret_dict = {}
        rcfile = config_path
        fh = open(config_path, "r")

        while line := fh.readline():
            line = re.sub(r"#.*$", "", line)  # Remove comments
            line = re.sub(r"^\s+", "", line)  # Remove leading whitespace
            if not line.strip():
                continue  # Skip blank lines

            if re.match(r"^global\s*$", line):
                key = "global"
            elif match := re.match(r"^(options|module)\s+([-/.\w]+)\s*$", line):  # Get modulename (has dash, dots, slashes, or letters/numbers)
                key = match.group(1) + " " + match.group(2)
            elif match := re.match(r"^module-set\s*([-/.\w]+)?\s*$", line):
                if match.group(1):
                    key = "module-set " + match.group(1)
                else:
                    key = "module-set " + f"Unnamed module-set at {rcfile}"  # module_set_name may be blank (because the capture group is optional)
            elif re.match(r"^\s*include\s+\S", line):
                line = line.rstrip("\n")
                match = re.match(r"^\s*include\s+(.+?)\s*$", line)
                filename = ""
                if match:
                    filename = match.group(1)
                filename = filename.replace("${module-definitions-dir}", "${build-configs-dir}")\
                    .replace("kf5-qt5.ksb", "kde5.yaml")\
                    .replace("kf6-qt6.ksb", "kde6.yaml")\
                    .replace("kf${_ver}-qt${_ver}.ksb", "kde${_ver}.yaml")\
                    .replace(".ksb", ".yaml")
                key = "include " + filename
                ret_dict[key] = ""  # use empty line as a value
                continue  # do not expect "end" marker after include line.
            else:
                print(f"Invalid configuration file {rcfile}!")
                print(f"Expecting a start of module section.")
                raise ValueError("Ungrouped/Unknown option")

            node = KSBParser._read_ksb_node(fh, rcfile)
            if key in ret_dict:
                msg = f"Duplicate entry \"{key}\" found in {rcfile}."
                if key.startswith("options "):
                    msg += " Note that \"options\" can be duplicated only in _different_ files."
                raise ValueError(msg)
            ret_dict[key] = node

        fh.close()
        return ret_dict

    @staticmethod
    def _read_ksb_node(file_handler: TextIO, file_name: str) -> dict:
        """
        Read in the options of the node (a section that is terminated with "end" word) from ksb file and construct dict.

        Args:
            file_handler: A file handle to read from.
            file_name: A full path for file name that is read.
        """
        end_re = re.compile(r"^\s*end")

        ret_dict = {}
        # Read in each option
        line = KSBParser._read_next_logical_line(file_handler)
        while line and not re.search(end_re, line):

            # Sanity check, make sure the section is correctly terminated
            if re.match(r"^(module\b|options\b)", line):
                print(f"Invalid configuration file {file_name}\nAdd an \"end\" before starting a new module.\n")
                raise ValueError(f"Invalid file {file_name}")

            option, value = KSBParser._split_option_and_value(line)
            ret_dict[option] = value
            line = KSBParser._read_next_logical_line(file_handler)

        return ret_dict

    @staticmethod
    def _read_next_logical_line(file_reader: TextIO) -> str | None:
        """
        Read a "line" from a file.

        This line is stripped of comments and extraneous whitespace. Also, backslash-continued multiple lines are merged into a single line.

        Args:
            file_reader: The reference to the filehandle to read from.

        Returns:
             The text of the line.
        """
        line = file_reader.readline()
        while line:
            # Remove trailing newline
            line = line.rstrip("\n")

            # Replace \ followed by optional space at EOL and try again.
            if re.search(r"\\\s*$", line):
                line = re.sub(r"\\\s*$", "", line)
                line = line.rstrip() + " "  # strip spaces on the right of the piece, add one space
                line += file_reader.readline().lstrip()  # strip spaces on the left of the piece
                continue

            if re.search(r"#.*$", line):
                line = re.sub(r"#.*$", "", line)  # Remove comments
            if re.match(r"^\s*$", line):
                line = file_reader.readline()
                continue  # Skip blank lines

            return line
        return None

    @staticmethod
    def _split_option_and_value(input_line: str) -> tuple:
        """
        Take an input line, and extract it into an option name and value.

        Args:
            input_line: The line to split.

        Returns:
             Tuple (option-name, option-value)
        """
        # The option is the first word, followed by the
        # flags on the rest of the line.  The interpretation
        # of the flags is dependent on the option.
        pattern = re.compile(
            r"^\s*"  # Find all spaces
            r"([-\w]+)"  # First match, alphanumeric, -, and _
            # (?: ) means non-capturing group, so (.*) is $value
            # So, skip spaces and pick up the rest of the line.
            r"(?:\s+(.*))?$"
        )

        match = re.match(pattern, input_line)
        option = match.group(1)
        value = match.group(2) or ""

        value = value.strip()

        return option, value


if __name__ == "__main__":

    def compare_two_configs(ksb_file, yaml_file):
        ksb_config_content = KSBParser.read_ksb_file(ksb_file)

        # Replace "true" and "false" strings to real boolean values
        for node in ksb_config_content:
            if not isinstance(ksb_config_content[node], dict):
                continue  # "include" lines are not dicts
            for option, value in ksb_config_content[node].items():
                if value == "true":
                    ksb_config_content[node][option] = True
                if value == "false":
                    ksb_config_content[node][option] = False

        # Rename entries: "module" -> "project"; "module-set" -> "group", "options" -> "override".
        old_keys = list(ksb_config_content.keys())
        for node in old_keys:
            new_name = None
            if node.startswith("module "):
                new_name = "project " + node.removeprefix("module ")
            if node.startswith("module-set "):
                new_name = "group " + node.removeprefix("module-set ")
            if node.startswith("options "):
                new_name = "override " + node.removeprefix("options ")

            # store the recognized node under new name, and remove old name
            if new_name:
                ksb_config_content[new_name] = ksb_config_content[node]
                del ksb_config_content[node]

        # Rename "use-modules" -> "use-projects"; "ignore-modules" -> "ignore-projects". Listify/dictify some options value.
        old_keys = list(ksb_config_content.keys())
        for node in old_keys:
            if not isinstance(ksb_config_content[node], dict):
                continue  # "include" lines are not dicts
            old_item_keys = list(ksb_config_content[node].keys())
            for option in old_item_keys:
                if option == "use-modules":
                    ksb_config_content[node]["use-projects"] = ksb_config_content[node]["use-modules"].split(" ")
                    del ksb_config_content[node]["use-modules"]
                if option == "ignore-modules":
                    ksb_config_content[node]["ignore-projects"] = ksb_config_content[node]["ignore-modules"].split(" ")
                    del ksb_config_content[node]["ignore-modules"]
                if option == "set-env":
                    value = ksb_config_content[node]["set-env"]
                    name, val = value.split(" ", maxsplit=1)
                    ksb_config_content[node]["set-env"] = {name: val}

        with open(yaml_file, "r") as file:
            yaml_config_content = yaml.safe_load(file)

        # Now, compare the two configs
        found_diff = False
        for options_name, options in ksb_config_content.items():
            print(f"\tChecking {options_name}")
            if isinstance(options, dict):
                for option, value in options.items():
                    if isinstance(ksb_config_content[options_name][option], list):  # for use-projects and ignore-projects
                        for el in ksb_config_content[options_name][option]:
                            if el not in yaml_config_content[options_name].get(option, []):

                                # Handling known differences
                                if el == "qtwebengine":
                                    print("\t\tIgnoring qtwebengine")
                                    continue
                                # End of handling known differences

                                found_diff = True
                                print("\t\tError: \"" + el + "\" is not contained in \"" + " ".join(yaml_config_content[options_name].get(option, [])) + "\"")
                                print(ksb_file, yaml_file)
                                sys.exit(1)
                    else:
                        if ksb_config_content[options_name][option] != yaml_config_content[options_name].get(option, None):
                            ### Manual handling the known differences
                            if options_name == "override appstream":
                                if option == "configure-flags":
                                    print("Ignoring difference in \"" + options_name + "\" for option \"" + option + "\"")
                                    continue
                            ### End of handling known differences
                            found_diff = True
                            print("\t\tError: \"" + ksb_config_content[options_name][option] + " is not equal " + yaml_config_content[options_name][option])
                            print(ksb_file, yaml_file)
                            sys.exit(1)
            else:
                if ksb_config_content[options_name] != yaml_config_content[options_name]:
                    found_diff = True
                    print("\t\tError: \"" + ksb_config_content[options_name] + " is not equal " + yaml_config_content[options_name])
                    print(ksb_file, yaml_file)
                    sys.exit(1)
        return not found_diff

    print("Making checks that changes to ksb configs are also applied to yaml configs.")
    mod_def_dir = os.getcwd() + "/module-definitions/"
    build_conf_dir = os.getcwd() + "/build-configs/"
    for file_name in os.listdir(mod_def_dir):
        if file_name.endswith('.ksb'):
            file_path = os.path.join(mod_def_dir, file_name)
            yaml_file_name = file_name.replace(".ksb", ".yaml")
            yaml_file_path = os.path.join(build_conf_dir, yaml_file_name)
            yaml_file_path = yaml_file_path.replace("kf5-qt5.yaml","kde5.yaml").replace("kf6-qt6.yaml","kde6.yaml")
        else:
            continue

        print(f"Checking {file_path} against {yaml_file_path}")
        compare_two_configs(file_path, yaml_file_path)
        pass
