# Module definitions for building KDE Workspace 6

# This file uses "branch groups" to decide which git branch to use. If you
# want to add your application here please be sure to update
# kde-build-metadata repo's "logical-module-structure". It includes a simple
# tool you can use to validate your change works (or just "kdesrc-build -p
# your-module" and look for the right branch).

module-set kf6-workspace-modules
    repository kde-projects
    cmake-options -DBUILD_WITH_QT6=ON -DBUILD_QT5=OFF
    use-modules \
      workspace \

    # modules not yet KF6-ready
    ignore-modules \
      breeze-grub \
      breeze-plymouth \
      khotkeys \
      plasma-mobile \
      plasma-settings \
      plasma-bigscreen \
      aura-browser \
      plank-player \
      plasma-remotecontrollers \
      plasma-meetings \

end module-set

# Left here instead of kf5-common-options.ksb as it is already tied
# to the rest of these workspace modules.
options breeze-plymouth
    # Prevent from trying to install breeze-text.so to /usr/lib/plymouth/, see https://invent.kde.org/plasma/breeze-plymouth/-/merge_requests/5
    cmake-options -DINSTALL_BROKEN_PLYMOUTH_DATA=ON
end options

options drkonqi
    # Disable testing until https://invent.kde.org/plasma/drkonqi/-/issues/4 is solved
    cmake-options -DBUILD_TESTING=OFF
end options

module-set mobile
    repository kde-projects
    use-modules \
      plasma-mobile \
      plasma-settings \

end module-set

module-set bigscreen
    repository kde-projects
    use-modules \
      plasma-bigscreen \
      aura-browser \
      plank-player \
      plasma-remotecontrollers \

end module-set

module-set kf5-baloo-widgets
    repository kde-projects
    # Temporarily here. Not workspace-specific.
    use-modules \
      baloo-widgets \

    cmake-options -DBUILD_WITH_QT6=ON
end module-set

module-set kf6-ws-kgad
    repository kde-projects
    # Temporarily here. Not workspace-specific, but needed by kwin
    use-modules \
      kglobalacceld \

    cmake-options -DBUILD_WITH_QT6=ON
end module-set

# kate: syntax kdesrc-buildrc;
