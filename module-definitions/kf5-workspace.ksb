# Module definitions for building KDE Workspace 5

# This file uses "branch groups" to decide which git branch to use. If you
# want to add your application here please be sure to update
# kde-build-metadata repo's "logical-module-structure". It includes a simple
# tool you can use to validate your change works (or just "kdesrc-build -p
# your-module" and look for the right branch).

module-set kf5-workspace-modules
    repository kde-projects # Required for branch-group

    # Compile everything under kde/workspace
    use-modules \
      workspace \

    # kdesrc-build can build dependencies (that it knows about) even if you forget
    # to list them all, if you uncomment this line.
    # include-dependencies true

    ignore-modules \
      kwindowsaddons \
      breeze-grub \
      plasma5support \
      plasma-meetings \

       # kwindowsaddons - Remove if you're somehow using Windows
       # breeze-grub - doesn't have a buildsystem
       # plasma5support - Qt6-only modules
       # plasma-meetings - No code there
end module-set

# Left here instead of kf5-common-options.ksb as it is already tied
# to the rest of these workspace modules.
options breeze-plymouth
    # Prevent from trying to install breeze-text.so to /usr/lib/plymouth/, see https://invent.kde.org/plasma/breeze-plymouth/-/merge_requests/5
    cmake-options -DINSTALL_BROKEN_PLYMOUTH_DATA=ON
end options

options drkonqi
    # Disable testing until https://invent.kde.org/plasma/drkonqi/-/issues/4 is solved
    cmake-options -DBUILD_TESTING=OFF
end options

# For some reason kwalletmanager is in kde/kdeutils, but is considered par of workspace
module-set kf5-workspace-utils
    repository kde-projects
    use-modules \
      kwalletmanager \

end module-set

module-set kf5-baloo-widgets
    repository kde-projects
    # Temporarily here. Not workspace-specific.
    use-modules \
      baloo-widgets \

end module-set

# kate: syntax kdesrc-buildrc
