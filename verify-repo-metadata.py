#!/usr/bin/env python3

import argparse
import os
import re
import sys

import yaml

# Gather the command line arguments we need
parser = argparse.ArgumentParser(description="Verifies the metadata files")
parser.add_argument("--metadata-path", help="Path to the metadata to check", required=True)
args = parser.parse_args()

# Make sure our configuration file exists
if not os.path.exists(args.metadata_path):
    print(f"Unable to locate specified metadata location: {args.metadata_path}")
    sys.exit(1)

# Regular expression used to validate project names that are valid for Gitlab itself
gitlab_project_name_regex = re.compile(r"^[\w\u00A9-\u1f9ff_][\w\-\u00A9-\u1f9ff_. ]*$", flags=re.UNICODE)

# Start a list of used project identifiers and names
project_identifiers_in_use = []
project_names_in_use = []
had_errors = False

# Start going over the location in question...
for current_path, subdirectories, files_in_folder in os.walk(args.metadata_path, topdown=False, followlinks=False):
    # Do we have a metadata.yaml file?
    if "metadata.yaml" not in files_in_folder:
        # We're not interested then....
        continue

    # Now that we know we have something to work with....
    # Let's load the current metadata up
    metadata_path = os.path.join(current_path, "metadata.yaml")
    with open(metadata_path, "r", encoding="utf-8") as metadataFile:
        metadata = yaml.safe_load(metadataFile)

    # Trim the base path to the metadata off to make it a bit nicer to read
    current_location = current_path[len(args.metadata_path):]

    # check sanity of the description and make sure that it is less than 250
    # (part of gitlab restriction)
    if metadata["description"] is not None and len(metadata["description"]) > 250:
        print(current_location + ": Project description is longer than 250 characters")

    # check the description and ensure that it is not empty
    # currently warning, but still best to list it out
    if metadata["description"] is not None and len(metadata["description"]) == 0:
        print(current_location + ": Project description is empty")

    # Check if name have supported characters only
    if gitlab_project_name_regex.match(metadata["name"]) is None:
        print(current_location + ": Project name contains characters not allowed by Gitlab - " + metadata["name"])

    # Make sure the name is not already in use
    if metadata["name"] in project_names_in_use:
        print(current_location + ": Project name is already in use - " + metadata["name"])

    # Make sure that identifier is not empty
    identifier = metadata.get("identifier")
    if identifier is None:
        print(current_location + ": CRITICAL ERROR - Project identifier is not set")
        sys.exit(1)

    # Make sure identifier is equal to the leaf of projectpath and repopath
    if not current_location.startswith("/websites") and not current_location.startswith("/sysadmin") and not current_location.startswith("/documentation") and not current_location.startswith("/neon"):
        projectpath = metadata.get("projectpath")
        repopath = metadata.get("repopath")
        if identifier != repopath.split("/")[1]:
            print(current_location + f": ERROR - Project identifier is not equal to repopath leaf: {identifier} {repopath}")
            had_errors = True
        if identifier != projectpath.split("/")[-1]:
            print(current_location + f": ERROR - Project identifier is not equal to projectpath leaf: {identifier} {projectpath}")
            had_errors = True

    # Make sure the identifier is not in use already
    if identifier in project_identifiers_in_use:
        print(current_location + ": CRITICAL ERROR - Project identifier is already in use - " + metadata["identifier"])
        sys.exit(1)

    # All checks done for this project
    # Add it to the list of identifiers and names in use
    project_names_in_use.append(metadata["name"])
    project_identifiers_in_use.append(identifier)

if had_errors:
    sys.exit(1)

# All done!
sys.exit(0)
